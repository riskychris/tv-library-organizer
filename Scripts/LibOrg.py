import sys
import os
import re
import pathlib
import subprocess
import json
import csv
import urllib.request
import urllib.error
import pickle
import cv2

root = sys.argv[1]
ShowKeys = {}

EpisodeNames = {}
EpisodeResolutions = {}

AttemptedRename = {}
AttemptedRes = {}

stagedChanges = {}




def getShowkey(show):

    if show not in ShowKeys.keys():
        showkey = show.lower()
        showkey = showkey.replace(" - ", "-")
        showkey = showkey.replace(' ', '-')
        showkey = showkey.replace('\'', "")
        showkey = showkey.replace('!', "")
        showkey = showkey.replace(',', "")
        showkey = showkey.replace(')', "")
        showkey = showkey.replace('(', "")
        if (show == "24"):
            showkey += "-1"
        ShowKeys[show] = showkey

    return ShowKeys[show]


def organize():
    for show in os.listdir(root):
        print("Scanning Episode Files:  " + show)

        showpath = root + show + "/"

        if os.path.isdir(showpath):
            organizeShow(show, showpath, 1)
        print()


def organizeShow(show, path, level):
    for file in os.listdir(path):
        if os.path.isdir(path + file):
            dirpath = path + file + "/"

            indent(level)
            print("Directory:  " + file)
            organizeShow(show, dirpath, level + 1)

        else:
            renameFile(show, file, path, level)


def renameFile(show, file, path, level):
    newName = getNewFileName(show, file, path, level)

    if newName == -1:
        indent(level)
        print("Could not find season/episode number for " + file)
        return

    season = getSeasonNumber(file)
    episode = getEpisodeNumber(file)

    seasonRes = getSeasonModeResolution(show, season)
    if seasonRes is None:
        seasonRes = ""
    if not seasonRes == "":
        seasonRes = "  " + seasonRes

    stagedChanges[path+file] = {"show":show, "season" : season, "episode" : episode, "name" : newName}

def getNewFileName(show, file, path, level):
    renameformats = [".mkv", ".avi", ".mp4", ".m4v", ".srt"]

    if pathlib.Path(file).suffix.lower() not in renameformats:
        return -1

    season = getSeasonNumber(file)
    episode = getEpisodeNumber(file)
    
    if episode is None or season is None:
        return -1

    epName = getEpisodeName(show, season, episode)
    epRes = getEpisodeResolution(show, season, episode, path)

    if epName is not None:
        epName = " -- " + epName
    else:
        epName = ""

    newFileName = show + "  S" + season + " E" + episode + epName + "  " +epRes + pathlib.Path(file).suffix

    return newFileName


def getEpisodeName(show, season, episode):
    global EpisodeNames
    
    showkey = getShowkey(show)
    if showkey not in EpisodeNames.keys():
        EpisodeNames[showkey] = {}

    name = None
    if season not in EpisodeNames[showkey].keys() or episode not in EpisodeNames[showkey][season].keys():
        EpisodeNames[showkey][season] = getSeasonEpisodeNames(show, season)

    if episode in EpisodeNames[showkey][season].keys():
        name = EpisodeNames[showkey][season][episode]

    return name


def getSeasonEpisodeNames(show, season):
    showkey = getShowkey(show)
    if showkey not in EpisodeNames.keys():
        EpisodeNames[showkey] = {}

    if showkey not in AttemptedRename.keys():
        AttemptedRename[showkey] = {}

    if season in AttemptedRename[showkey].keys():
        return {}

    AttemptedRename[showkey][season] = "True"

    tableBody = requestSeasonFromTVDB(show, season)
    if tableBody is None:
        return {}

    namelist = {}
    for row in tableBody.split('<tr>'):
        cells = row.split('<td>')

        if len(cells) < 3:
            continue
        # The first row is always empty, followed by ep. num, ep. name
        numcell = cells[1]
        namecell = cells[2]

        num = numcell.split(">")[1].split("<")[0]
        name = namecell.split(">")[2].split("<")[0]

        # whitespace
        num = int(num.strip())
        name = name.strip()

        if num < 10:
            num = "00" + str(num)
        elif num < 100:
            num = "0" + str(num)
        else:
            num = str(num)

        # illegal characters
        name = name.replace('\'', "")
        name = name.replace("&#39;", "")  # HTML single quote
        name = name.replace('?', "")
        name = name.replace(":", " -")
        name = name.replace("*", "_")
        name = name.replace("/", " and ")
        name = name.replace("\"", "")
        name = name.replace("&#39;", "")

        # meaningless, clutter names
        namelist[num] = name

    return namelist

def requestSeasonFromTVDB(show, season):
    badformat = ["Animaniacs", "Boruto - Naruto Next Generation", "Naruto", "Naruto Shippuden", "Doctor Who",
                 "The Tenth Kingdom"]

    if show in badformat:
        return None

    showkey = getShowkey(show)
    url = "http://thetvdb.com/series/" + showkey + "/seasons/" + str(season)
    q = urllib.request

    try:
        f = q.urlopen(url)
    except urllib.error.HTTPError as e:
        # Return code error (e.g. 404, 501, ...)
        print('HTTPError: {}'.format(e.code))
        return None
    except urllib.error.URLError as e:
        # Not an HTTP-specific error (e.g. connection refused)
        print('URLError: {}'.format(e.reason))
        return None

    html = f.read().decode("utf-8")
    html = html[html.find("<tbody>") + 8:html.find("/tbody>")]

    return html

def getSeasonsInSeries(show):
    badformat = ["Animaniacs", "Boruto - Naruto Next Generation", "Naruto", "Naruto Shippuden", "Doctor Who",
                 "The Tenth Kingdom"]

    if show in badformat:
        return None

    showkey = getShowkey(show)
    url = "http://thetvdb.com/series/" + showkey + "/seasons/all"
    q = urllib.request

    try:
        f = q.urlopen(url)
    except urllib.error.HTTPError as e:
        # Return code error (e.g. 404, 501, ...)
        print('HTTPError: {}'.format(e.code))
        return None
    except urllib.error.URLError as e:
        # Not an HTTP-specific error (e.g. connection refused)
        print('URLError: {}'.format(e.reason))
        return None

    html = f.read().decode("utf-8")
    return len(html.split("<tbody>")) - 2 #minus the specials

def getEpisodeResolution(show, season, episode, parentDir):
    global EpisodeResolutions
    
    showkey = getShowkey(show)
    if showkey not in EpisodeResolutions.keys():
        EpisodeResolutions[showkey] = {}

    if season not in EpisodeResolutions[showkey].keys():
        EpisodeResolutions[showkey][season] = {}

    if season not in EpisodeResolutions[showkey].keys() or episode not in EpisodeResolutions[showkey][season].keys():
        if parentDir not in AttemptedRes.keys():
            currSeasonRes = EpisodeResolutions[showkey][season]
            addSeasonRes = getSeasonResolutions(show, season, parentDir)
            EpisodeResolutions[showkey][season] = {**currSeasonRes, **addSeasonRes}

    if episode not in EpisodeResolutions[showkey][season].keys():
        res = ""
    else:
        res = EpisodeResolutions[showkey][season][episode]

    return res


def getSeasonResolutions(show, season, seasonPath):
    seasonResList = {}
    seasonPath = seasonPath.lower()

    if seasonPath in AttemptedRes.keys():
        return seasonResList

    AttemptedRes[seasonPath] = "True"

    print("\t\tStarting Fast-Resolution Method")
    if seasonPath.find("shippuden") != -1:
        print("\t\tUnable to get resolution with fast method")
        return cv2Res(seasonPath)


    cmds = [ '../MediaTools/exiftool.exe', '-j', '-DisplayHeight', "-DisplayWidth", seasonPath]
    p = subprocess.Popen(cmds, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    info, error = p.communicate()
    info = info.decode("utf-8")
    if not len(info) > 4:
        return cv2Res(seasonPath)
    info = json.loads(info)

    for data in info:
        file = data["SourceFile"].split("/")
        file = file[len(file)-1]

        episode = getEpisodeNumber(file)

        if "DisplayHeight" in data.keys():
            height = data["DisplayHeight"]
        else:
            continue

        if "DisplayHeight" in data.keys():
            width = data["DisplayWidth"]
        else:
            print("No resolution data for: " + episode)
            continue

        res =  dimensionsToRes(height, width)
        seasonResList[episode]= res


    # if no average season resolution, get resolutions with ffprobe
    if seasonResList == {}:
        print("\t\tUnable to get resolution with fast method")
        return cv2Res(seasonPath)

    return seasonResList

def cv2Res(seasonPath):
    seasonResList = {}
    print("\n\t\tStarting CV2-Resolution Method")
    for episode in os.listdir(seasonPath):
        renameformats = [".mkv", ".avi", ".mp4", ".m4v"]

        if pathlib.Path(episode).suffix.lower() not in renameformats:
            continue

        vidcap = cv2.VideoCapture(seasonPath + "/" +episode)
        success, image = vidcap.read()
        if image is None:
            continue
        height,width,channels = image.shape
        episode = getEpisodeNumber(episode)

        res = dimensionsToRes(height, width)
        seasonResList[episode] = res

    if seasonResList == {} :
        print("\t\tUnable to get resolution with CV2 method")
        return GetFFprobeResolutions(seasonPath)

    return seasonResList

def dimensionsToRes(height, width):
    if abs(width - 1920) < 100 and abs(height - 1080) < 50:
        return "(1080p)"

    if abs(width - 1280) < 50 and abs(height - 720) < 50:
        return "(720p)"

    elif abs(height - 576) < 30:
        return "(576p)"

    elif abs(height - 480) < 60:
        return "(480p)"

    elif abs(height - 360) < 60:
        return "(360p)"

    elif abs(height - 240) < 30:
        return "(240p)"

    elif height != 0:
        height = int(height / 10)
        height = height * 10
        return "(" + str(height) + "p)"

    return ""

def getSeasonModeResolution(show, season):
    showkey = getShowkey(show)
    if showkey not in EpisodeResolutions.keys():
        return ""

    if season not in EpisodeResolutions[showkey].keys():
        return ""

    modeList = [0]
    for episode in EpisodeResolutions[showkey][season].keys():
        res = EpisodeResolutions[showkey][season][episode]
        modeList.append(res)

    mode = max(set(modeList), key=modeList.count)
    if mode == 0:
        return ""
    return mode

def GetFFprobeResolutions(seasonPath):
    height = 0
    width  = 0
    seasonResList = {}
    print("\n\t\tStarting Slow-Resolution Method\n")

    for episode in os.listdir(seasonPath):

        episodepath = seasonPath + "/" + episode

        cmds = ['../MediaTools/ffprobe.exe',
                '-v', 'fatal', "-probesize", "32", '-show_entries', 'stream=width,height', '-of',
                'json', episodepath, '-sexagesimal', '-fflags', "nobuffer"]
        p = subprocess.Popen(cmds, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        info, error = p.communicate()
        info = json.loads(info.decode("utf-8"))

        if "streams" not in info.keys():
            print("\t\tNo video resolution info for: " + os.path.split(episodepath)[1])
            continue
        streams =  info["streams"]

        for i in range(0, len(streams)):
            if "height" in streams[i].keys() and "width" in streams[i].keys():
                width = streams[i]["width"]
                height = streams[i]["height"]
                break
            i += 1

        episode = getEpisodeNumber(episode)

        res = dimensionsToRes(height, width)
        seasonResList[episode] = res

    return seasonResList

def getSeasonNumber(seasondir):
    Sn = re.compile('(S\s*[0-9]+|Season\s*[.]*[0-9]|[0-9]+\s*x\s*[0-9]?[0-9]+)', re.IGNORECASE)

    # find Season-Number_string
    Slist = Sn.findall(seasondir)
    if len(Slist) == 0:
        return None

    li = Slist[0]
    if not li.find('x') == -1:
        li = li[li.find('x') + 1:]


    # find where the letters stop/numbers start
    numstart = 0
    while not li[numstart].isdigit():
        numstart += 1



    # extract season number from season-string
    S = li[numstart:]

    i = 0
    while i + 1 < len(S):
        if not S[i].isdigit():
            S = S[0:i] + S[i + i:]
        i += 1
    S = int(S)

    # if it's less than 10, add a 0 to the front
    if S < 10:
        S = "0" + str(S)
    else:
        S = str(S)

    return S


def getEpisodeNumber(episode):
    En = re.compile(
        '([0-9]E\s*[.]*[0-9]+|\s+E\s*\.?[0-9]+|Ep\s*[.]*[0-9]+|Episode\s*[.]*[0-9]+|[0-9]+\s*x\s*[0-9]?[0-9])',
        re.IGNORECASE)
    Elist = En.findall(episode)
    if len(Elist) == 0:
        Elist = re.findall('[0-9]?[0-9]?[0-9]', episode)

    if len(Elist) == 0:
        return None

    li = Elist[0].lower()
    if not li.find('x') == -1:
        li = li[li.find('x') + 1:]
    if not li.find('e') == -1:
        li = li[li.find('e') + 1:]

    # loop until first digit
    numstart = 0
    while not li[numstart].isdigit():
        numstart += 1

    E = int((li[numstart:]))

    if E < 10:
        E = "00" + str(E)
    elif E < 100:
        E = "0" + str(E)
    else:
        E = str(E)

    return E


def stageChanges():
    pickle.dump(stagedChanges, open("../Data/Changes.p", "wb"))
    pickle.dump(EpisodeResolutions, open("../Data/Resolutions.p", "wb"))
    pickle.dump(EpisodeNames, open("../Data/Names.p", "wb"))

    for oldPath in stagedChanges.keys():
        if not os.path.exists(oldPath):
            continue

        info = stagedChanges[oldPath]
        show = info["show"]
        season = info["season"]
        name = info["name"]

        seasonRes = getSeasonModeResolution(show, season)
        if seasonRes is not "":
            seasonRes = "  " + seasonRes

        seasonDir = root + show + "/Season " + season + seasonRes + "/"
        newPath = seasonDir + name
        stagedChanges[oldPath]["seasonDir"] = seasonDir
        stagedChanges[oldPath]["newPath"] = newPath

        if not oldPath.lower() == newPath.lower():
            print("Found:  " + oldPath)
            print("Naming: " + newPath)
            print()

    agree = ""
    while agree != "I am satisfied with the irreversible name changes about to take place":
        agree = input(
            "Please type:\n\t\tExit\t\tor\t\tI am satisfied with the irreversible name changes about to take place\n")
        if agree.lower() == "exit":
            break
    if agree == 'exit':
        print("Rename Cancelled")
        return -1

    for oldPath in stagedChanges.keys():
        if not os.path.exists(oldPath):
            continue

        seasonDir = stagedChanges[oldPath]["seasonDir"]
        newPath = stagedChanges[oldPath]["newPath"]

        if  oldPath.lower() == newPath.lower():
            continue

        if not os.path.exists(seasonDir):
            os.mkdir(seasonDir)

        if not os.path.exists(newPath):
            os.rename(oldPath, newPath)

    print("Successfully Renamed")
    print("\n\nCleaning Empty Directories")
    dirCleanup(root, 0)
    print("Creating a spreadsheet list of your shows")
    listShows()

def dirCleanup(path, level):

    for file in os.listdir(path):
        if not os.path.isdir(path + file):
            continue
        filepath = path + file + "/"

        if level == 0:
            print("\nCleaning folders in: " + path, end='')
        else:
            indent(level)
        print(file)

        dirCleanup(filepath, level + 1)

        if len(os.listdir(filepath)) == 0:
            os.rmdir(filepath)
            for i in range(0, level):
                print('\t', end='')
            print("Removing empty directory: " + filepath)
            print()
            continue


def listShows():
    TVpath = root
    showlistpath = TVpath + "TV Show List.csv"
    showlist = open(showlistpath, 'w')
    showWriter = csv.writer(showlist, lineterminator='\n')

    for show in os.listdir(TVpath):
        showdir = TVpath + show

        if not os.path.isdir(showdir):
            continue

        seasoncount = 0
        for season in os.listdir(showdir):
            if season.find("Season") == -1:
                continue
            seasoncount += 1
        seasons = str(seasoncount) + " Seasons"
        if seasons == 0:
            seasons = "no season structure"
            missing = ""
        else:
            totalSeasons = getSeasonsInSeries(show)
            if totalSeasons is not None:
                numMissing = totalSeasons - seasoncount
                missing = "Missing " + str(numMissing) + " seasons"

        print(show + " (" + seasons + ")" + " (" + missing + ")")
        showWriter.writerow([show, seasons])


def indent(level):
    for i in range(0, level):
        print('\t', end='')


def main():
    global EpisodeNames
    global EpisodeResolutions
    global stagedChanges
    global root

    root = sys.argv[1]
    if os.path.exists("../Data/Names.p"):
        EpisodeNames = pickle.load(open("../Data/Names.p", "rb"))

    if os.path.exists("../Data/Resolutions.p"):
        EpisodeResolutions = pickle.load(open("../Data/Resolutions.p", "rb"))
        print("Loaded")
        '''
       
        for show in EpisodeResolutions.keys():
            print(show)
            for season in EpisodeResolutions[show].keys():
                print("\t" + season)
                print("\t\t", end='')
                for episode in EpisodeResolutions[show][season].keys():
                    print(EpisodeResolutions[show][season][episode], end='')
                print()
                print("\t" + getSeasonModeResolution(show, season))
                print()
        '''
    if os.path.exists("../Data/Changes.p"):
        recommit = ""
        while recommit.lower() not in ["yes", "y", "no", "n"]:
            recommit = input(
                "Would you like to reommit changes from the last run?")
        if recommit in ["yes", "y"]:
            stagedChanges = pickle.load(open("../Data/Changes.p", "rb"))
            stageChanges()
            return

    organize()

    print("Listing Changes:")
    stageChanges()

main()