import sys
import os
from decimal import Decimal
import copy
import pathlib
import heapq

fileSizes = {}
maxSizeHeap = []
disks = []
root = sys.argv[1]


class DiskDAO:
    def __init__(self, num, capacity):
        self.num = num
        self.capacity = Decimal(0.9*capacity)
        self.free = Decimal(0.9*capacity)
        self.files = []

    def print(self):
        print("Disk " + str(self.num))
        for file in self.files:
            print("\t" + file)
        print("Free: " + str(self.free))
        print()

    def __lt__(self, other):
        return self.free < other.free

    def __le__(self, other):
        return self.free <= other.free



class DirDAO:
    def __init__(self, name, size):
        self.name = name
        self.size = size

    def __lt__(self, other):
        return -self.size < - other.size

    def __le__(self, other):
        return -self.size <= -other.size

    def __str__(self):
        return self.name + ": " + str(self.size) + " GB"


def main():
    getShowSizes()
    calculateDisksNeeded()

def calculateDisksNeeded():
    count = 0
    capacity = 50
    realCap = Decimal(.9*capacity)

    while len(maxSizeHeap) > 0:
        dir = heapq.heappop(maxSizeHeap)
        subcount = 1
        print(str(dir))
        while dir.size > realCap:
            disk = DiskDAO(count, capacity)
            disk.free = 0
            disk.files.append(dir.name + "(Part " +str(subcount) + ")")
            subcount+=1
            dir.size -= realCap
            disks.append(disk)
            count += 1


        fname = dir.name
        if subcount != 1:
            fname += "(Part " + str(subcount) + ")"

        inserted = False
        temp = copy.copy(disks)
        heapq.heapify(temp)

        while len(temp) > 0:
            disk = heapq.heappop(temp)
            if dir.size <= disk.free:
                disk.files.append(fname)
                disk.free -= dir.size
                inserted = True
                break

        if not inserted:
            disk = DiskDAO(count, capacity)
            disk.files.append(fname)
            disk.free -= dir.size
            disks.append(disk)
            count+=1


    for disk in disks:
        disk.print()


def getShowSizes():
    for show in os.listdir(root):
        print("Naming:  " + show)

        showpath = root + show + "/"

        if os.path.isdir(showpath):
            dirSize = Decimal(getDirSize(show, showpath, 1, 0))
            dirSize = round(dirSize / 1073741824, 2)
            DAO = DirDAO(show, dirSize)
            heapq.heappush(maxSizeHeap, DAO)


def getDirSize(show, path, level, size):
    for file in os.listdir(path):
        if os.path.isdir(path + file):
            dirpath = path + file + "/"
            indent(level)
            print("Directory: "+ file + ": ")
            size += getDirSize(show, dirpath, level+1, 0)

        else:
            size += os.path.getsize(path + file)

    return size

def indent(level):
    for i in range(0, level):
        print('\t', end='')

main()